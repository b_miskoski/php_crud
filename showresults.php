<!doctype html>
<html>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Form results</title>
  </head>
  <body>

    <!-- Jumbotron greatings/info -->
    <div class="container">
    <div class="jumbotron text-center">
      <h1 class="display-4">Entry results</h1>
      <p class="lead">The information from the entry results are displyed in the table bellow.</p>
      <a href="insert.php" class="btn btn-primary">Enter another record</a>
    </div>
    </div>

<?php
  // requires the process.php file that contains the db connection
  include 'process.php';

  // query for presenting data in a table, also for grouping the additional phone numbers for the users who have more then one phone
  $result = $mysqli->query("SELECT personal.id, personal.first_name, personal.last_name, personal.email, personal.phone_number, GROUP_CONCAT(phone_number2) as phone_number2 FROM personal LEFT JOIN phone ON personal.id = phone.user_id GROUP BY personal.id") or die($mysqli->error);

?>
<!-- Basic container, storing a table for data presentation -->
<div class="container table-hover  table-sm">
  <table class="table">
      <tr class="thead-dark">
        <th>First name</th>
        <th>Last name</th>
        <th>Email</th>
        <th>Phone number</th>
        <th>Additional phone numbers</th>
        <th colspan="2" class="text-center">Action</th>
      </tr>
<?php
  // adding array value to the $row variable
  while ($row = $result->fetch_assoc()):


?>
      <tr>
        <!-- printing the data in the table -->
        <td><?php echo $row['first_name'];?></td>
        <td><?php echo $row['last_name'];?></td>
        <td><?php echo $row['email'];?></td>
        <td><?php echo $row['phone_number'];?></td>
        <td><?php echo $row['phone_number2'];?></td>
        <td>
            <a href="edit.php?edit=<?php echo $row['id']; ?>"
            class="btn btn-info">Edit</a>
            <a href="showresults.php?delete=<?php echo $row['id']; ?>"
            class="btn btn-danger">Delete</a>
        </td>
      </tr>
  <?php
    endwhile;
  ?>
  </table>
</div>

<div style="height: 50px"></div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
  </body>
</html>
