<?php

  include 'process.php';
?>

<!DOCTYPE html>
<html>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <title>Edit user</title>
  </head>
  <body>
    <!-- Jumbotron greatings/info -->
    <div class="container">
    <div class="jumbotron text-center">
      <h1 class="display-4">Edit user</h1>
      <p class="lead">Please edit your personal information</p>
    </div>
  </div>

  <div class="container">
    <form class="col-md-8 col-sm-8 col-8" id="userform" name="form" action="showresults.php" method="post">
      <div class="form-group">
        <label class="col-lg">Name *</label>
          <input type="text" id="first_name" name="first_name" class="form-control" value="<?php echo $first_name; ?>" placeholder="Enter your name">
      </div>

      <div class="form-group">
        <label class="col-lg">Last name *</label>
          <input type="text" id="last_name" name="last_name" class="form-control" value="<?php echo $last_name; ?>" placeholder="Enter your last name" >
      </div>

      <div class="form-group">
        <label class="col-lg">Email *</label>
          <input type="email" id="email" name="email" class="form-control" value="<?php echo $email; ?>" placeholder="Enter your email" >
      </div>

      <div id="tel_number" class="form-group">
        <table class="table">
          <tr>
              <label>Phone numbers *</label>
              <input type="tel" id="phone_number" name="phone_number" class="form-control col-md-12" value="<?php echo $phone_number; ?>" placeholder="Enter your phone number" >
          </tr>
        </table>
        <table class="table">
          <tr>
              <?php if ($phone_number >= 1): ?>
                <?php foreach ($result2 as $key => $value): ?>
                  <?php foreach ($value as $phone2 => $phone): ?>
                  <input type="tel" id="phone_number2" name="phone_number2[]" class="form-control col-md-12" value="<?php echo $phone; ?>" placeholder="Enter your phone number" >
                  <?php endforeach ?>
                <?php endforeach ?>
              <?php endif ?>
          </tr>
        </table>
      </div>
      <input type="hidden" name="update_id" value="<?php echo $id; ?>">
      <button type="submit" class="btn btn-outline-primary col-lg" name="update">Update</button>
      </form>
  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </body>

</html>
