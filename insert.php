<!doctype html>
<html>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <title>Insert user</title>

  </head>
  <body>

    <!-- Jumbotron greatings/info -->
    <div class="container">
    <div class="jumbotron text-center">
      <h1 class="display-4">Welcome</h1>
      <p class="lead">Please enter your personal information</p>
    </div>
  </div>
    <!-- Grid of 8 by 4 containing insert form and additional info -->
    <div class="container">
        <div class="row">
          <form class="col-md-8 col-sm-8 col-8" id="userform" name="form" action="process.php" method="post">
            <div class="form-group">
              <label class="col-lg">Name *</label>
                <input type="text" id="first_name" name="first_name" class="form-control" value="" placeholder="Enter your name">
            </div>

            <div class="form-group">
              <label class="col-lg">Last name *</label>
                <input type="text" id="last_name" name="last_name" class="form-control" value="" placeholder="Enter your last name" >
            </div>

            <div class="form-group">
              <label class="col-lg">Email *</label>
                <input type="email" id="email" name="email" class="form-control" value="" placeholder="Enter your email" >
            </div>

            <div id="tel_number" class="form-group">
              <table class="table">
                <tr>
                    <label>Phone number *</label>
                    <input type="tel" id="phone_number" name="phone_number" class="form-control col-md-12" value="" placeholder="Enter your phone number" >
                </tr>
                <tr>
                    <button type="button" id="add_num" name="add_num" class="btn btn-info col-md-4">Add more numbers</button>
              </tr>
              </table>
            </div>
            <button type="submit" class="btn btn-outline-primary col-lg" name="submit">Submit</button>
            </form>

            <div class="col-md-4 col-sm-4 col-4">
              <p>Please fill out all of the form fields marked as required (*). Fill out the form with truthful, and up to date informations.</p>
              <a href="showresults.php" class="btn btn-outline-success col lg">View users</a>
            </div>
        </div>
      </div>

      <div style="height: 50px"></div>
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <!-- Jquery script for adding multiple number fields -->
      <script>
      $(document).ready(function(){
        var i=1;
        $('#add_num').click(function(){
          i++;
          $('#tel_number').append('<div id="tel_number2" class="form-group"><table class="col-md-12"><tr><td><input type="tel" id="phone_number2" name="phone_number2[' + i + ']" class="form-control" value="" placeholder="Enter another phone number"></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">x</button></td></tr></table></div>');
          $('#phone_number2').each(function () {
          $.validator.addMethod('positiveNumber',function (value, element) {
                return this.optional(element) || /^\+?[0-9]*\.?[0-9]+$/.test(value);
          }, "Only valid phone numbers accepted.");
            $(this).rules("add", {
              required: true,
              positiveNumber: true
            });
          });
        });
        $(document).on('click', '.btn_remove', function(){
      		var button_id = $(this).attr("id");
      		$('#tel_number2').remove();
      	});
      });
      </script>

      <script>
      $(document).ready(function(){

      $.validator.addMethod("lettersOnly", function(value, element) {
          return this.optional(element) || /^[a-z\u0400-\u04FF]+$/i.test(value);
        }, "Letters only please");

      $.validator.addMethod('positiveNumber',function (value, element) {
          return this.optional(element) || /^\+?[0-9]*\.?[0-9]+$/.test(value);
      }, "Only valid phone numbers accepted.");

          $("#userform").validate({
            rules: {
              first_name: {
                required: true,
                lettersOnly: true
              },
              last_name: {
                required: true,
                lettersOnly: true
              },
              email: {
                required: true,
                email: true
              },
              phone_number: {
                required: true,
                positiveNumber: true
              }
            },
            messages: {
              first_name: {
                required: "Please enter your name!",
                lettersOnly: "Letters only please"
              },
              last_name: {
                required: "Please enter your last name!",
                lettersOnly: "Letters only please"
              },
              email: {
                required: "Please enter your email address"
              },
              phone_number: {
                required: "Please enter your phone number"
              }
            }
          });
        });

      </script>
  </body>

</html>
