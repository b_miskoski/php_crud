<?php
session_start();

// Defined as constants so that they can't be changed
DEFINE ('DB_HOST', 'localhost');
DEFINE ('DB_USER', 'root');
DEFINE ('DB_PASSWORD', 'root');
DEFINE ('DB_NAME', 'exercise');

// Makes connection to the database
$mysqli = @mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME) or die($mysqli->error);

// Checks if the submit button has been pressed, setst the variable values of the name values from the form
if (isset($_POST['submit'])){
  $first_name = $_POST['first_name'];
  $last_name = $_POST['last_name'];
  $email = $_POST['email'];
  $phone_number = $_POST['phone_number'];

// Inserts the form input into the personal table in the exercise database
$mysqli->query("INSERT INTO personal (first_name, last_name, email, phone_number) VALUES ('$first_name', '$last_name', '$email', '$phone_number')")
      OR die($mysqli->error);
  $user_id = $mysqli->insert_id;

// Checks if the Add more numbers button has been pressed
 if (isset($_POST['phone_number2'])) {
   $phone_number2 = $_POST['phone_number2'];

// foreach statement to loop trought the array of inserted additional phone numbers
  foreach ($phone_number2 as $key => $value) {
      $mysqli->query("INSERT INTO phone (user_id, phone_number2) VALUES('$user_id','$value')")
               OR die($mysqli->error);
        }
    }
    header("Location: insert.php");
  }

// Checks if the delete button has been pressed
   if (isset($_GET['delete'])){
     $id = $_GET['delete'];
     $mysqli->query("DELETE FROM personal WHERE id=$id") or die($mysqli->error());
    }

// Checks if the edit button has been pressed
  if (isset($_GET['edit'])) {
    $id = $_GET['edit'];

    $result = $mysqli->query("SELECT * FROM personal where id=$id") or die($mysqli->error);

    while ($row = mysqli_fetch_assoc($result)) {

      $id = $row['id'];
      $first_name = $row['first_name'];
      $last_name = $row['last_name'];
      $email = $row['email'];
      $phone_number = $row['phone_number'];

      $result2 = $mysqli->query("SELECT phone.phone_number2 FROM phone where user_id=$id") or die($mysqli->error);

    } while ($row2 = mysqli_fetch_assoc($result2)) {

      $phone_number2 = $row2['phone_number2'];
    }
  }

  if (isset($_POST['update'])) {

    $update_id = $_POST['update_id'];
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $email = $_POST['email'];
    $phone_number = $_POST['phone_number'];

    if (empty($_POST['phone_number2'])) {
        $phone_number2 = '';
        $mysqli->query("UPDATE personal SET first_name='$first_name', last_name='$last_name', email='$email', phone_number='$phone_number' WHERE id={$update_id}") or die($mysqli->error);
    } else {
      $phone_number2 = $_POST['phone_number2'];
      foreach ($phone_number2 as $phone) {
          $result = $mysqli->query("UPDATE phone SET phone_number2='$phone' WHERE user_id={$update_id}") or die($mysqli->error);
      }

      $mysqli->query("UPDATE personal SET first_name='$first_name', last_name='$last_name', email='$email', phone_number='$phone_number' WHERE id={$update_id}") or die($mysqli->error);
    }
  }
